//
//  CellForTypes.h
//  TaxiNow
//
//  Created by Elluminati Macbook Pro 2 on 7/12/16.
//  Copyright © 2016 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForTypes : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@end
