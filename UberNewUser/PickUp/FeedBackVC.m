//
//  FeedBackVC.m
//  UberNewUser
//
//  Created by Elluminati on 01/11/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "UberStyleGuide.h"
#import "subTypeCell.h"

@interface FeedBackVC ()
{
    NSMutableArray *arrSubTypes;
    NSString *strForCurrency;
    UIButton *button;
    BOOL paid;
    NSString *strForPaypalMail;
}

@end

@implementation FeedBackVC

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ratingView initRateBar];
    strForCurrency = [dictBillInfo valueForKey:@"currency"];
    [self SetLocalization];
    self.navigationController.navigationBarHidden=NO;
    NSArray *arrName=[self.strFirstName componentsSeparatedByString:@" "];
    self.lblFirstName.text=[NSString stringWithFormat:@"%@ %@",[arrName objectAtIndex:0],[arrName objectAtIndex:1]];
    //self.lblDistance.text=[NSString stringWithFormat:@"%.2f",[[self.dictWalkInfo valueForKey:@"distance"] floatValue]];
    // self.lblTIme.text=[self.dictWalkInfo valueForKey:@"time"];
    
    self.lblDistance.text=[NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"distance"] floatValue],[dictBillInfo valueForKey:@"unit"]];
    self.lblTIme.text=[NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"time" ] floatValue],NSLocalizedString(@"Mins", nil)];
    
    [self.imgUser applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.imgUser downloadFromURL:self.strUserImg withPlaceholder:nil];
    self.viewForBill.hidden=NO;
    self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setPriceValue];
    [self customSetup];
}

- (void)viewDidAppear:(BOOL)animated
{
    // [self customSetup];
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    [self.btnFeedBack setTitle:NSLocalizedString(@" Invoice", nil) forState:UIControlStateNormal];
}

-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lBasePrice.text=NSLocalizedString(@"BASE PRICE", nil);
    self.lDistanceCost.text=NSLocalizedString(@"DISTANCE COST", nil);
    self.lTimeCost.text=NSLocalizedString(@"TIME COST", nil);
    self.lPromoBonus.text=NSLocalizedString(@"PROMO BOUNCE", nil);
    self.lreferalBonus.text=NSLocalizedString(@"REFERRAL BOUNCE", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    self.lComment.text=NSLocalizedString(@"COMMENT1", nil);
    self.lblPaymentModeTitle.text=NSLocalizedString(@"Payment Mode", nil);
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.btnFeedBack addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)checkPayment
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please wait......"];
        NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[PREF objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 [APPDELEGATE hideLoadingView];
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     dictBillInfo = [response valueForKey:@"bill"];
                     
                     strForPaypalMail = [response valueForKey:@"client_id"];
                     
                     if(strForPaypalMail.length>1)
                     {
                         paid = [[dictBillInfo valueForKey:@"is_paid"] intValue];
                         [self parallelPayment];
                     }
                     else
                     {
                         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please ask client to add his paypal id" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                         [alert show];
                     }
                 }
                 NSString *str = [response valueForKey:@"error_code"];
                 if([str intValue] == 406)
                 {
                     [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                 }
             }
         }];
    }
}

- (void)parallelPayment
{
    if(paid)
    {
        [button setHidden:YES];
        [self.btnFeedBack setTitle:@"  Feedback" forState:UIControlStateNormal];
        self.viewForBill.hidden=YES;
        self.navigationController.navigationBarHidden=NO;
        ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(140, 25) AndPosition:CGPointMake(90, 215)];
        ratingView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:ratingView];
    }
    else
    {
        NSLog(@"%f",[[dictBillInfo valueForKey:@"total"] floatValue]);
        if([[dictBillInfo valueForKey:@"total"] floatValue]>0)
        {
            [APPDELEGATE showLoadingWithTitle:@"Please wait.."];
            [self paypalPayment];
        }
        else
        {
            [button setHidden:YES];
            [self.btnFeedBack setTitle:@"  Feedback" forState:UIControlStateNormal];
            self.viewForBill.hidden=YES;
            self.navigationController.navigationBarHidden=NO;
            ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(140, 25) AndPosition:CGPointMake(90, 215)];
            ratingView.backgroundColor=[UIColor clearColor];
            [self.view addSubview:ratingView];
        }
    }
}

#pragma mark-
#pragma mark- Set Invoice Details

-(void)setPriceValue
{
    self.lblBasePrice.text=[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dictBillInfo valueForKey:@"base_price"] floatValue]];
    self.lblDistCost.text=[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dictBillInfo valueForKey:@"distance_cost"] floatValue]];
    self.lblTimeCost.text=[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dictBillInfo valueForKey:@"time_cost"] floatValue]];
    self.lblTotal.text=[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dictBillInfo valueForKey:@"total"] floatValue]];
    self.lblRferralBouns.text=[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dictBillInfo valueForKey:@"referral_bonus"] floatValue]];
    self.lblPromoBouns.text=[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dictBillInfo valueForKey:@"promo_bonus"] floatValue]];
    self.lblDistance.text=[NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"distance"] floatValue],[dictBillInfo valueForKey:@"unit"]];
    
    float totalDist=[[dictBillInfo valueForKey:@"distance_cost"] floatValue];
    float Dist=[[dictBillInfo valueForKey:@"distance"]floatValue];
    
    if([[dictBillInfo valueForKey:@"payment_type"]intValue]==1)
    {
        self.lblPaymentMode.text = NSLocalizedString(@"Cash Payment", nil);
    }
    else
    {
        self.lblPaymentMode.text = NSLocalizedString(@"Card Payment", nil);
    }
    if ([[dictBillInfo valueForKey:@"unit"]isEqualToString:@"kms"])
    {
        totalDist=totalDist*0.621317;
        Dist=Dist*0.621371;
    }
    if(Dist!=0)
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"%@%.2f %@",strForCurrency,(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
    }
    else
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"%@ %@",strForCurrency,NSLocalizedString(@"per mile", nil)];
    }
    
    float totalTime=[[dictBillInfo valueForKey:@"time_cost"] floatValue];
    float Time=[[dictBillInfo valueForKey:@"time"]floatValue];
    if(totalTime/Time!=0)
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"%@%.2f%@",strForCurrency,(totalTime/Time),NSLocalizedString(@"per mins", nil)];
    }
    else
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"%@ %@",strForCurrency,NSLocalizedString(@"per mins", nil)];
    }
    if([[dictBillInfo valueForKey:@"is_type_flat"] integerValue]==1)
    {
        self.viewForDetailBill.hidden=YES;
        self.tableForTypes.hidden=NO;
    }
    else
    {
        self.viewForDetailBill.hidden=NO;
        self.tableForTypes.hidden=YES;
    }
    
    arrSubTypes = [[NSMutableArray alloc]init];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"type"]];
    
    NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"PROMO_BONUS", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"promo_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"REFERRAL_BONUS", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"referral_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.lblDistance.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblDistCost.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblBasePrice.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblDistance.font=[UberStyleGuide fontRegular:15.0f];
    self.lblPerDist.font=[UberStyleGuide fontRegularLight:10.30f];
    self.lblPerTime.font=[UberStyleGuide fontRegularLight:10.30f];
    self.lblTIme.font=[UberStyleGuide fontRegular:15.0f];
    self.lblTimeCost.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblTotal.font=[UberStyleGuide fontRegular:42.0f];
    self.lblFirstName.font=[UberStyleGuide fontRegularLight:20.0f];
    self.lblLastName.font=[UberStyleGuide fontRegularLight:20.0f];
    // self.btnFeedBack.titleLabel.font=[UberStyleGuide fontRegularLight:20.0f];
    self.btnFeedBack=[APPDELEGATE setBoldFontDiscriptor:self.btnFeedBack];
    self.btnSubmit=[APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];
    
    self.lblPromoBouns.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblRferralBouns.font=[UberStyleGuide fontRegularLight:20.70f];
    self.btnConfirm.titleLabel.font=[UberStyleGuide fontRegularBold];
    self.btnSubmit.titleLabel.font=[UberStyleGuide fontRegularBold];
    
    self.lBasePrice.font = [UberStyleGuide fontRegularLight];
    self.lDistanceCost.font = [UberStyleGuide fontRegularLight];
    self.lTimeCost.font = [UberStyleGuide fontRegularLight];
    self.lreferalBonus.font = [UberStyleGuide fontRegularLight];
    self.lPromoBonus.font = [UberStyleGuide fontRegularLight];
    
}

#pragma mark - Paypal Integration

- (BOOL)acceptCreditCards
{
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

#pragma mark - Paypal Integration

- (void)paypalPayment
{
    [APPDELEGATE hideLoadingView];
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"YOUR_CLIENT_ID_FOR_PRODUCTION",
                                                           PayPalEnvironmentSandbox :strForPaypalMail}];
    _payPalConfig = [[PayPalConfiguration alloc] init];
#if HAS_CARDIO
    _payPalConfig.acceptCreditCards = YES;
#else
    _payPalConfig.acceptCreditCards = NO;
#endif
    _payPalConfig.merchantName = @"Moto Help Uk";
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    self.environment = kPayPalEnvironment;
    [self setPayPalEnvironment:self.environment];
    
    NSMutableArray *arrItem = [[NSMutableArray alloc]init];
    arrItem = [[dictBillInfo valueForKey:@"type"] valueForKey:@"name"];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    NSDecimalNumber *subtotal = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%@",[[dictBillInfo valueForKey:@"total"] stringValue]]];
    
    payment.amount = subtotal;
    
    payment.currencyCode = strForCurrency;
    
    payment.shortDescription = [arrItem objectAtIndex:0];
    
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment configuration:self.payPalConfig
                                                                                                     delegate:self];
    
    [self presentViewController:paymentViewController animated:YES completion:nil];
}

#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    NSLog(@"PayPal Payment Success!");
    
    [self sendCompletedPaymentToServer:completedPayment];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment
{
    // TODO: Send completedPayment.confirmation to server
    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    
    NSString *strPaypalId = [[completedPayment.confirmation valueForKey:@"response"] valueForKey:@"id"];
    
    if([completedPayment.confirmation isKindOfClass:[NSDictionary class]])
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"PaymentWithPaypal"];
        if([APPDELEGATE connected])
        {
            NSString *strId=[PREF objectForKey:PREF_USER_ID];
            NSString *strToken=[PREF objectForKey:PREF_USER_TOKEN];
            NSString *strRequest_id=[PREF objectForKey:PREF_REQ_ID];
            [APPDELEGATE showLoadingWithTitle:@"Please wait...."];
            
            NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
            [dictParams setValue:strId forKey:PARAM_ID];
            [dictParams setValue:strToken forKey:PARAM_TOKEN];
            [dictParams setValue:strRequest_id forKey:PARAM_REQUEST_ID];
            [dictParams setValue:strPaypalId forKey:@"paypal_id"];
            
            NSLog(@"dictonary parameter for payment by payal :%@",dictParams);
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_PAYPAL_PAYMENT withParamData:dictParams withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [button setHidden:YES];
                         [self.btnFeedBack setTitle:@"  Feedback" forState:UIControlStateNormal];
                         self.viewForBill.hidden=YES;
                         self.navigationController.navigationBarHidden=NO;
                         
                         ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(140, 25) AndPosition:CGPointMake(90, 215)];
                         ratingView.backgroundColor=[UIColor clearColor];
                         [self.view addSubview:ratingView];
                         
                         NSLog(@"Payal API Response - %@",response);
                         [APPDELEGATE showToastMessage:@"Congratulations, your payment is successfully done!"];
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                     }
                     else
                     {
                         NSString *str = [response valueForKey:@"error_code"];
                         if([str intValue] == 406)
                         {
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                     }
                 }
                 else if (error)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Internal Server Error"
                                                                         message:@"Your Payment has been recieved by PayPal. Once the services has been restored the same will be reflected in History"
                                                                        delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alertView show];
                 }
             }];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self
                                               cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Some Problem occured while Payment please , try again .." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}

#pragma mark-
#pragma makr- Btn Click Events

- (IBAction)onClickBackBtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)submitBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        
        [self.txtComments resignFirstResponder];
        RBRatings rating=[ratingView getcurrentRatings];
        float rate=rating/2.0;
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_RATE", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        else
        {
            NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[PREF objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:strForUserId forKey:PARAM_ID];
            [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setObject:strReqId forKey:PARAM_REQUEST_ID];
            [dictParam setObject:[NSString stringWithFormat:@"%f",rate] forKey:PARAM_RATING];
            NSString *commt=self.txtComments.text;
            if([commt isEqualToString:NSLocalizedString(@"COMMENT", nil)])
            {
                [dictParam setObject:@"" forKey:PARAM_COMMENT];
            }
            else
            {
                [dictParam setObject:self.txtComments.text forKey:PARAM_COMMENT];
            }
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"%@",response);
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                         [PREF removeObjectForKey:PREF_REQ_ID];
                         
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         NSString *str = [response valueForKey:@"error_code"];
                         if([str intValue] == 406)
                         {
                             [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                         }
                     }
                 }
                 
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"subType";
    
    subTypeCell *cell = [self.tableForTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [arrSubTypes objectAtIndex:indexPath.row];
    
    cell.lblTypeName.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]];
    cell.lblPrice.text = [NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dict valueForKey:@"price"] floatValue]];
    
    cell.lblTypeName.font=[UberStyleGuide fontRegularLight:16.70f];;
    cell.lblPrice.font=[UberStyleGuide fontRegularLight:16.70f];;
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSubTypes.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark -
#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)confirmBtnPressed:(id)sender
{
    if([[dictBillInfo valueForKey:@"payment_type"]isEqualToString:@"1"])
    {
        [self.btnFeedBack setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
        self.viewForBill.hidden=YES;
        self.navigationController.navigationBarHidden=NO;
        
        ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(140, 25) AndPosition:CGPointMake(90, 215)];
        ratingView.backgroundColor=[UIColor clearColor];
        [self.view addSubview:ratingView];
    }
    else
    {
        [self checkPayment];
    }
}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.txtComments.text=@"";
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                    toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                    toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    if ([self.txtComments.text isEqualToString:@""])
    {
        self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    }
    
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
